import { StyleSheet, Platform } from "react-native";
import color from '../../common/Color'
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.LightYellow,
  },
  showinfor: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: Platform.OS = 'ios' ? 50 : 0,
    backgroundColor: color.white,
    paddingVertical: 10,
  },
  icUser: {
    width: 40,
    height: 40,
    marginStart: 10
  },
  sizeImage: {
    width: 30,
    height: 30,
  },
  borderImage: {
    width: 40,
    height: 40,
    alignItems:'center',
    justifyContent:'center',
    borderRadius:10,
    backgroundColor:color.blue,
  },
  textUser: {
    paddingHorizontal: 10,
  },
  MyOrder: {
    marginTop: 10,
    backgroundColor: color.white,
    padding: 10,
  },
  titleOrder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleBonus: {
    marginTop:10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding:10,
    backgroundColor:color.white,
    alignItems:'center'
  },
  titleComment: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding:10,
    backgroundColor:color.white,
    alignItems:'center'
  },
  titleGift:{
    flexDirection:'row',
    alignItems:'center'
  },
  bodyOrder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  bodyOrder1: {
    flex: 1,
    alignItems: 'center',
    marginTop:10,
  },
  text: {
    textAlign: 'center',
    fontSize:12,
  },
  itemList:{
    width:230,
    borderRadius: 5,
    backgroundColor:color.white,
    margin: 10,
    padding:10,
  },
  textProduct:{
    marginLeft:10,
    marginRight:20,
    fontSize:12,
    color:color.gray,
  },
  imageStar:{
    width: 10, 
    height: 10,
    marginLeft:5,
  },
  addComment:{
    marginTop:10,
    height:30,
    borderRadius:5,
    borderWidth:1,
    borderColor:color.blue,
    justifyContent:'center',
    alignItems:'center',
  }

})

export default styles;