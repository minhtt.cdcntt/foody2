import React from "react";
import { View, Text, TouchableOpacity, Alert, Image, ScrollView, FlatList } from "react-native";
import styles from "./Styles";
import { Card } from 'react-native-shadow-cards';
import color from '../../common/Color'

const DATA = [
    {
        id: 1,
        nameproduct: "Laptop Lenovo WinBook 100e (gen 2) Intel N4020/4GB/64GB/11.6inch HD/W10 giá siêu rẻ cho học sinh - Hàng nhập khẩu",
        image: "https://salt.tikicdn.com/cache/280x280/ts/product/08/31/a0/0ceba5b0d78306e4fbdbd89f369e4e12.jpg",
    },
    {
        id: 2,
        nameproduct: "Laptop Lenovo WinBook 100e (gen 2) Intel N4020/4GB/64GB/11.6inch HD/W10 giá siêu rẻ cho học sinh - Hàng nhập khẩu",
        image: "https://salt.tikicdn.com/cache/280x280/ts/product/08/31/a0/0ceba5b0d78306e4fbdbd89f369e4e12.jpg",
    },
    {
        id: 3,
        nameproduct: "Laptop Lenovo WinBook 100e (gen 2) Intel N4020/4GB/64GB/11.6inch HD/W10 giá siêu rẻ cho học sinh - Hàng nhập khẩu",
        image: "https://salt.tikicdn.com/cache/280x280/ts/product/08/31/a0/0ceba5b0d78306e4fbdbd89f369e4e12.jpg",
    },
    {
        id: 4,
        nameproduct: "Laptop Lenovo WinBook 100e (gen 2) Intel N4020/4GB/64GB/11.6inch HD/W10 giá siêu rẻ cho học sinh - Hàng nhập khẩu",
        image: "https://salt.tikicdn.com/cache/280x280/ts/product/08/31/a0/0ceba5b0d78306e4fbdbd89f369e4e12.jpg",
    }
]



const About = ({ navigation, route }) => {
    const userDetail = () => {
        // Alert.alert('to page detail');
    }


    const renderItem = ({ item }) => {
        return (
            <Card style={styles.itemList}>
                <View>
                    <View style={{ flexDirection: 'row' }}>
                        <Image
                            style={styles.sizeImage}
                            source={{
                                uri: item.image,
                            }}
                        />
                        <View>
                            <Text numberOfLines={1} style={styles.textProduct}>{item.nameproduct}</Text>
                            <View style={{ flexDirection: 'row', marginTop: 5, marginLeft: 10 }}>
                                <Image source={require('../image/star.png')} style={styles.imageStar} />
                                <Image source={require('../image/star.png')} style={styles.imageStar} />
                                <Image source={require('../image/star.png')} style={styles.imageStar} />
                                <Image source={require('../image/star.png')} style={styles.imageStar} />
                            </View>
                        </View>
                    </View>
                    <View>
                        <Text style={{ fontStyle: 'italic', fontSize: 10, color: color.gray, marginTop: 5, }}>"Hữu ích hơn khi đánh giá có nội dung"</Text>
                        <TouchableOpacity>
                            <View style={styles.addComment}>
                                <Text>Thêm nhận xét/ảnh</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
            </Card>
        )
    }

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={userDetail}>
                <View style={styles.showinfor}>
                    <Image
                        source={require('../image/iconuser.png')} style={styles.icUser} resizeMethod='scale' />
                    <View style={styles.textUser}>
                        <Text>Tran Thanh Minh</Text>
                        <Text>Xem trang ca nhan</Text>
                    </View>
                </View>
            </TouchableOpacity>

            <View style={styles.MyOrder}>
                <View style={styles.titleOrder}>
                    <Text>Đơn hàng của tôi</Text>
                    <TouchableOpacity>
                        <Text style={{ color: '#10B8F0' }}>Xem lịch sử</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.bodyOrder}>
                    <View style={styles.bodyOrder1}>
                        <View style={styles.borderImage}>
                            <Image source={require('../image/iconuser.png')} style={styles.sizeImage} />
                        </View>
                        <Text style={styles.text}>Chờ thanh toán</Text>
                    </View>
                    <View style={styles.bodyOrder1}>
                        <View style={styles.borderImage}>
                            <Image source={require('../image/iconuser.png')} style={styles.sizeImage} />
                        </View>
                        <Text style={styles.text}>Đang xử lý</Text>
                    </View>
                    <View style={styles.bodyOrder1}>
                        <View style={styles.borderImage}>
                            <Image source={require('../image/iconuser.png')} style={styles.sizeImage} />
                        </View>
                        <Text style={styles.text}>Đang vận chuyển</Text>
                    </View>
                    <View style={styles.bodyOrder1}>
                        <View style={styles.borderImage}>
                            <Image source={require('../image/iconuser.png')} style={styles.sizeImage} />
                        </View>
                        <Text style={styles.text}>Chờ đánh giá</Text>
                    </View>

                </View>
            </View>
            <View style={styles.titleBonus}>
                <View style={styles.titleGift}>
                    <Image source={require('../image/gift.png')} style={{ width: 20, height: 20 }} />
                    <Text style={{ marginLeft: 5 }}>Săn thưởng</Text>
                </View>
                <Image source={require('../image/next.png')} />
            </View>

            <View style={{ marginTop: 10, backgroundColor: 'white' }}>
                <View style={styles.titleComment}>
                    <View style={styles.titleGift}>
                        <Image source={require('../image/gift.png')} style={{ width: 20, height: 20 }} />
                        <Text style={{ marginLeft: 5 }}>Đánh giá sản phẩm</Text>
                    </View>
                    <Image source={require('../image/next.png')} />
                </View>
                <FlatList
                    data={DATA}
                    renderItem={renderItem}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                />
            </View>

            <View style={styles.titleBonus}>
                <View style={styles.titleGift}>
                    <Image source={require('../image/gift.png')} style={{ width: 20, height: 20 }} />
                    <Text style={{ marginLeft: 5 }}>Sản phẩm đã mua</Text>
                </View>
                <Image source={require('../image/next.png')} />
            </View>

        </View>

    )
}

export default About;