import React from "react";
import { View, Text, TouchableOpacity, Alert } from "react-native";


const Setting = ({navigation,route}) => {
    const{name} = route.params;
    return (
        <View>
            <TouchableOpacity>
                <Text>Setting {name}</Text>
            </TouchableOpacity>
        </View>
    )
}

export default Setting;