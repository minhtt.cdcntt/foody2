import { StyleSheet } from "react-native";
import color from "../../common/Color";

const Styles = StyleSheet.create({
    contrainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: color.yellow,
        paddingHorizontal:20,
    },
    borderInput: {
        width: '100%',
        height: 40,
        backgroundColor: color.white,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: color.red,
    },

    borderLogin: {
        marginTop:10,
        width: '50%',
        height: 40,
        backgroundColor: color.green,
        borderRadius: 10,
        justifyContent:'center',
        alignItems:'center'
    },
    text:{
        color: color.white,
        fontSize:15,
    }
})

export default Styles;