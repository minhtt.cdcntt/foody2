import React, { useState } from "react";
import { View, Text, TouchableOpacity, Alert, TextInput } from "react-native";
import styles from "./Styles";

const Login = ({ navigation,route }) => {
    const [user, setUser] = useState('');
    const [pass, setPass] = useState('');
    const handleLogin = () => {
        if (user.length == 0) {
            Alert.alert('Vui long nhap tai khoan')
        } else if (pass.length == 0) {
            Alert.alert('Vui long nhap mat khau')
        } else {
            navigation.navigate('MyTabs', { name: 'Jane' })
        }
    }
    return (
        <View style={styles.contrainer}>
            <TextInput placeholder="nhap tai khoan" style={styles.borderInput} onChangeText={text => setUser(text)} />
            <TextInput placeholder="nhap mat khau" style={styles.borderInput} onChangeText={text => setPass(text)} secureTextEntry={true} />
            <TouchableOpacity onPress={handleLogin} style={styles.borderLogin}>
                <Text style={styles.text}>Dang nhap</Text>
            </TouchableOpacity>
        </View>
    )
}

export default Login;