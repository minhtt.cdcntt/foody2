export default{
    white:'#fff',
    red:'red',
    yellow:'yellow',
    green:'green',
    LightYellow:'#F2F2F2',
    blue:'#A9E2F3',
    gray:'#848484',
};