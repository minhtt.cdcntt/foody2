import React,{component} from "react";
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Login from './view/LoginScreen/Login';
import Setting from './view/SettingScreen/Setting';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from "./view/HomeScreen/Home";
import About from "./view/AboutScreen/About";



const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MyTabs =(props) => {
  return (
    <Tab.Navigator screenOptions={{headerShown:false}}>
      <Tab.Screen name="Home"  children ={()=><Home {...props}/>}  options={{title:'Home'}}/>
      <Tab.Screen name="Setting" children ={()=><Setting {...props}/>}/>
      <Tab.Screen name="About" children ={()=><About {...props}/>}/>
    </Tab.Navigator>
  );
}

const MyStack = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown:false}}>
                <Stack.Screen name="Login" component={Login}  options={{title:"Đăng nhập"}}/>
                <Stack.Screen name="MyTabs" component={MyTabs}  options={{title:"Cài đặt"}}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}
export default MyStack;
